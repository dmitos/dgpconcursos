import sys
import filecmp
import os
import shutil
import requests


#bajo el archivo desde la pagina.
#y guardo con el nombre del dia de bajado.
class archivo:
    def bajar(self, direccion, nombre):
        r = requests.get(direccion)
        with open(nombre, 'wb') as f:
            f.write(r.content)
            print("descarga completa--- ok")

    def leerarchivo(self):
        archivo = open('ultimo/archivo.txt', 'r')
        ultimafecha = archivo.read(8)
        archivo.close()
        return(ultimafecha)

    def comprobando(self, viejo, nombre, dia):
        comp = filecmp.cmp(viejo, nombre)
        if comp is True:
            os.remove(nombre)
            return("No hay actualizacion del pdf")
        else:
            shutil.copy2(nombre, "pdf/" + dia + ".pdf")
            os.remove(nombre)
            archivo = open('ultimo/archivo.txt', 'w')
            archivo.write(dia)
            archivo.close()
            os.system("start " + "pdf/" + dia + ".pdf")
            return("Se actualizo el archivo pdf %s" % dia)


def reporte(count, blockSize, totalSize):
    percent = int(count * blockSize * 100 / totalSize)
    if percent <= 100:
        sys.stdout.write("\n...%3d%%" % percent)
        sys.stdout.flush()
    else:
        print("\n...100%")
